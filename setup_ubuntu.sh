# Ubuntu updates:
sudo apt update

# net-tools:
sudo apt install net-tools

# gridsite-clients:
sudo apt -y install gridsite-clients

# git:
sudo apt -y install git
## git config: https://blog.pragtechnologies.com/ubuntu-dev-setup/
git config --global push.default simple 
git config --global user.email "bryan_dagasdas@medilink.com.ph" 
git config --global user.name "Bryan Dagasdas"  
## bash-git-prompt: https://github.com/magicmonty/bash-git-prompt
git clone https://github.com/magicmonty/bash-git-prompt.git ~/.bash-git-prompt --depth=1
## Add to the ~/.bashrc:
echo 'if [ -f "$HOME/.bash-git-prompt/gitprompt.sh" ]; then' >> ~/.bashrc
echo '    GIT_PROMPT_ONLY_IN_REPO=1' >> ~/.bashrc
echo '    source $HOME/.bash-git-prompt/gitprompt.sh' >> ~/.bashrc
echo 'fi' >> ~/.bashrc
source ~/.bashrc

# docker: https://docs.docker.com/install/linux/docker-ce/ubuntu/
sudo apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io
## docker post-install: https://docs.docker.com/install/linux/linux-postinstall/
sudo groupadd docker
sudo usermod -aG docker $USER

# docker-compose: https://docs.docker.com/compose/install/
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# python3 pip and virtualenv:
sudo apt -y install python3-pip python3-virtualenv
python3 -m pip install --upgrade pip

# chromium: https://snapcraft.io/chromium
sudo snap install chromium

# visual studio code: https://snapcraft.io/code
sudo snap install code --classic

# dbeaver: https://snapcraft.io/dbeaver-ce
sudo snap install dbeaver-ce

# postman: https://snapcraft.io/postman
sudo snap install postman

# Microsoft ODBC Driver 17 for SQL Server: https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver15
sudo curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
## Ubuntu 19.10
sudo curl -o /etc/apt/sources.list.d/mssql-release.list https://packages.microsoft.com/config/ubuntu/19.10/prod.list
sudo apt-get update
sudo ACCEPT_EULA=Y apt-get -y install msodbcsql17
sudo apt-get -y install libodbc1 unixodbc odbcinst1debian2 unixodbc-dev

# Set Ubuntu favorite apps
gsettings set org.gnome.shell favorite-apps "['org.gnome.Terminal.desktop', 'org.gnome.Nautilus.desktop', 'chromium_chromium.desktop', 'code_code.desktop', 'dbeaver-ce_dbeaver-ce.desktop', 'postman_postman.desktop', 'org.gnome.Software.desktop', 'org.gnome.gedit.desktop']"

# Clone ubuntu repo from gitlab.com to home
git clone https://gitlab.com/bryan_dagasdas/ubuntu ~/ubuntu
shopt -s dotglob && cp -vfr ~/ubuntu/* ~/
rm -rf ~/ubuntu

# Setup hosts file
sudo cp ~/hosts /etc/hosts