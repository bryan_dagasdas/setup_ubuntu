## Setup Ubuntu
1. Download setup script
 ```sh
 wget -O ~/setup_ubuntu.sh https://gitlab.com/bryan_dagasdas/setup_ubuntu/-/raw/master/setup_ubuntu.sh
 ```

2. Execute setup script
 ```sh
 source ~/setup_ubuntu.sh
 ```
